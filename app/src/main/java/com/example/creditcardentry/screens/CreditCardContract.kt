package com.example.creditcardentry.screens

interface CreditCardContract {
    interface View {
        fun showSuccess()
        fun showErrorCvv()
        fun showErrorInvalidCardNumber()
        fun showErrorExpirationDate()
        fun errorCreditNumber()
        fun errorCreditExpirationDate()
        fun errorCreditCvv()

        fun displayImageAmex()
        fun displayImageDinersClub()
        fun displayImageDiscover()
        fun displayImageJcb()
        fun displayImageVisa()
        fun displayImageMastercard()
        fun displayImageGeneric()
        fun hideImage()

        fun creditNumberSetText(chars: CharSequence?)
        fun creditExpDateSetText(chars: CharSequence?)
        fun creditCvvSetText(chars: CharSequence?)

        fun setPresenter(presenter: Presenter)
    }

    interface Presenter {
        fun isCreditNumberLastCharValid(chars: CharSequence?): Boolean
        fun creditNumberStringUpdated(chars: CharSequence?)
        fun creditCvvStringUpdated(chars: CharSequence?)
        fun creditExpirationDateStringUpdated(chars: CharSequence?)
        fun bindView(view: View)
        fun unbindView()
        fun submitClicked(number: CharSequence)
    }
}