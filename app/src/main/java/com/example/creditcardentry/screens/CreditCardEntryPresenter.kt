package com.example.creditcardentry.screens

import androidx.annotation.VisibleForTesting

open class CreditCardEntryPresenter : CreditCardContract.Presenter {

    private var creditCardHelper: CreditCardHelper = CreditCardHelper()

    private var view: CreditCardContract.View? = null

    private var numberOfMaxCharsWithoutNetwork = 6
    private var numberOfMaxCharsNetworkAmex = 15
    private var numberOfMaxCharsNetworkAll = 16


    constructor(view: CreditCardContract.View) {
        this.view = view
        this.view?.setPresenter(this)
    }

    override fun bindView(view: CreditCardContract.View) {
        this.view = view
    }

    override fun unbindView() {
        view = null
    }

    override fun isCreditNumberLastCharValid(chars: CharSequence?): Boolean {
        if (chars == null || chars.isEmpty()) {
            return false
        }
        return chars.last().isDigit()
    }

    override fun creditNumberStringUpdated(chars: CharSequence?) {
        if (chars == null) {
            return
        }

        if (!isCreditNumberLastCharValid(chars)) {
            view?.creditNumberSetText(trimLastChar(chars))
        }

        val networkType = creditCardHelper.getCardNetwork(chars)
        if (chars.length > numberOfMaxCharsWithoutNetwork && networkType == CreditCardHelper.Companion.Network.GENERIC) {
            view?.creditNumberSetText(chars.substring(0, chars.length - 1))
        }

        if (chars.length > numberOfMaxCharsNetworkAmex && networkType == CreditCardHelper.Companion.Network.AMEX) {
            view?.creditNumberSetText(chars.substring(0, chars.length - 1))
        } else if (chars.length > numberOfMaxCharsNetworkAll) {
            view?.creditNumberSetText(chars.substring(0, chars.length - 1))
        }

        when (networkType) {
            CreditCardHelper.Companion.Network.AMEX -> view?.displayImageAmex()
            CreditCardHelper.Companion.Network.DINERS_CLUB -> view?.displayImageDinersClub()
            CreditCardHelper.Companion.Network.DISCOVER -> view?.displayImageDiscover()
            CreditCardHelper.Companion.Network.JCB -> view?.displayImageJcb()
            CreditCardHelper.Companion.Network.VISA -> view?.displayImageVisa()
            CreditCardHelper.Companion.Network.MASTERCARD -> view?.displayImageMastercard()
            CreditCardHelper.Companion.Network.GENERIC -> {
                view?.displayImageGeneric()
                //chars = chars.removeRange(chars.length - 1..chars.length)
            }
        }
    }

    override fun creditCvvStringUpdated(chars: CharSequence?) {
        if (chars == null || chars.isEmpty()) {
            return
        }

        if (!chars.last().isDigit()) {
            view?.creditCvvSetText(trimLastChar(chars))
        }

        if (chars.length > 3) {
            view?.creditCvvSetText(trimLastChar(chars))
            view?.errorCreditCvv()
        }

    }

    override fun creditExpirationDateStringUpdated(chars: CharSequence?) {
        if (chars == null || chars.isEmpty()) {
            return
        }

        if (chars.length == 1) {
            var ch = chars.last()
            if (!ch.isDigit() && (ch.toInt() != 0 || ch.toInt() != 1)) {
                view?.creditExpDateSetText(trimLastChar(chars))
            }
        }

        if (chars.length == 2) {
            var ch = chars.last()
            if (!ch.isDigit() && ch.toInt() !in 1..9) {
                view?.creditExpDateSetText(trimLastChar(chars))
            }
        }

        if (chars.length == 3) {
            if (chars.last() != '/') {
                view?.creditExpDateSetText(trimLastChar(chars))
                view?.showErrorExpirationDate()
            }
        }

        if (chars.length in 4..5) {
            if (!chars.last().isDigit()) {
                view?.creditExpDateSetText(trimLastChar(chars))
            }
        }

        if (chars.length > 5) {
            view?.creditExpDateSetText(trimLastChar(chars))
        }
    }

    override fun submitClicked(chars: CharSequence) {
        if (creditCardHelper.isCardNumberValid(chars)) {
            view?.showSuccess()
        } else {
            view?.showErrorInvalidCardNumber()
        }
    }

    @VisibleForTesting
    fun trimLastChar(chars: CharSequence) : CharSequence {
        if (chars.isNotEmpty()) {
            return chars.substring(0, chars.length - 1)
        }
        return chars
    }
}