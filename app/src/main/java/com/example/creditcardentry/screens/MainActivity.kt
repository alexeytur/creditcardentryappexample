package com.example.creditcardentry.screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.creditcardentry.R

class MainActivity : AppCompatActivity() {
    private lateinit var presenter: CreditCardContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var creditCardFragment = supportFragmentManager.findFragmentById(R.id.credit_card_container)
        if (creditCardFragment == null) {
            creditCardFragment = CreditCardFragment.getInstance()
        }

        setFragment(R.id.credit_card_container, creditCardFragment)
        presenter = CreditCardEntryPresenter(creditCardFragment as CreditCardContract.View)

    }

    private fun setFragment(fragId: Int, fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(fragId, fragment).commit()
    }
}
