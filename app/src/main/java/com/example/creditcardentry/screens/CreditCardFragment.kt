package com.example.creditcardentry.screens

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.creditcardentry.R

class CreditCardFragment : Fragment(), CreditCardContract.View {

    private lateinit var creditCardNumberTextEdit: EditText
    private lateinit var creditCardExpirationDateTextEdit: EditText
    private lateinit var creditCardCvvTextEdit: EditText
    private lateinit var creditCardLogo: ImageView
    private lateinit var creditSubbitButton: Button

    private var creditCardPresenter: CreditCardContract.Presenter? = null

    private lateinit var textWatcherCardNumber: TextWatcher
    private lateinit var textWatcherCardExpDate: TextWatcher
    private lateinit var textWatcherCardCvv: TextWatcher

    private val KEY_CARD_NUMBER = "ket_bundle_card_number"

    companion object {
        fun getInstance(): CreditCardFragment {
            return CreditCardFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var root: View = inflater.inflate(R.layout.fragment_credit_card_entry, container, false)

        var keyNumberString = ""
        if (savedInstanceState?.get(KEY_CARD_NUMBER) != null) {
            keyNumberString = savedInstanceState.get(KEY_CARD_NUMBER) as String
        }
        initViews(root, keyNumberString)

        return root
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_CARD_NUMBER, creditCardNumberTextEdit.text.toString())
    }

    private fun initViews(rootView: View, cardNumberString: String) {
        creditCardNumberTextEdit = rootView.findViewById(R.id.cre_card_number)
        creditCardExpirationDateTextEdit = rootView.findViewById(R.id.cre_expiration_date)
        creditCardCvvTextEdit = rootView.findViewById(R.id.cre_cvv)
        creditCardLogo = rootView.findViewById(R.id.cre_network_image)
        creditSubbitButton = rootView.findViewById(R.id.cre_btn_submit)

        creditSubbitButton.setOnClickListener{
            creditCardPresenter?.submitClicked(creditCardNumberTextEdit.text)
        }

        textWatcherCardNumber = object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(charSeq: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(charSeq: CharSequence?, p1: Int, p2: Int, p3: Int) {
                creditCardPresenter?.creditNumberStringUpdated(charSeq)
            }
        }

        textWatcherCardExpDate = object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(charSeq: CharSequence?, p1: Int, p2: Int, p3: Int) {
                creditCardPresenter?.creditExpirationDateStringUpdated(charSeq)
            }
        }

        textWatcherCardCvv = object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(charSeq: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(charSeq: CharSequence?, p1: Int, p2: Int, p3: Int) {
                creditCardPresenter?.creditCvvStringUpdated(charSeq)
            }
        }

        creditCardNumberTextEdit.addTextChangedListener(textWatcherCardNumber)
        creditCardExpirationDateTextEdit.addTextChangedListener(textWatcherCardExpDate)
        creditCardCvvTextEdit.addTextChangedListener(textWatcherCardCvv)

        if (cardNumberString.isNotEmpty()) {
            creditCardPresenter?.creditNumberStringUpdated(cardNumberString)
        }
    }

    override fun creditNumberSetText(chars: CharSequence?) {
        creditCardNumberTextEdit.removeTextChangedListener(textWatcherCardNumber)
        creditCardNumberTextEdit.setText(chars)
        creditCardNumberTextEdit.setSelection(chars?.length!!)
        creditCardNumberTextEdit.addTextChangedListener(textWatcherCardNumber)
    }

    override fun creditExpDateSetText(chars: CharSequence?) {
        creditCardExpirationDateTextEdit.removeTextChangedListener(textWatcherCardExpDate)
        creditCardExpirationDateTextEdit.setText(chars)
        creditCardExpirationDateTextEdit.setSelection(chars?.length!!)
        creditCardExpirationDateTextEdit.addTextChangedListener(textWatcherCardExpDate)
    }

    override fun creditCvvSetText(chars: CharSequence?) {
        creditCardCvvTextEdit.removeTextChangedListener(textWatcherCardCvv)
        creditCardCvvTextEdit.setText(chars)
        creditCardCvvTextEdit.setSelection(chars?.length!!)
        creditCardCvvTextEdit.addTextChangedListener(textWatcherCardCvv)
    }

    override fun showSuccess() {
        Toast.makeText(this.context, getString(R.string.msg_success), Toast.LENGTH_SHORT).show()
    }

    override fun showErrorCvv() {
        Toast.makeText(this.context, getString(R.string.msg_error_cvv), Toast.LENGTH_SHORT).show()
    }

    override fun showErrorInvalidCardNumber() {
        Toast.makeText(this.context, getString(R.string.msg_error_invalid_card_number), Toast.LENGTH_SHORT).show()
    }

    override fun showErrorExpirationDate() {
        Toast.makeText(this.context, getString(R.string.msg_error_exp_date), Toast.LENGTH_SHORT).show()
    }

    override fun setPresenter(presenter: CreditCardContract.Presenter) {
        creditCardPresenter = presenter
    }

    override fun errorCreditNumber() {
    }

    override fun errorCreditExpirationDate() {

    }

    override fun errorCreditCvv() {
    }

    override fun displayImageAmex() {
        creditCardLogo.setImageResource(R.drawable.cards_amex)
    }

    override fun displayImageDinersClub() {
        creditCardLogo.setImageResource(R.drawable.cards_dinerclub)
    }

    override fun displayImageDiscover() {
        creditCardLogo.setImageResource(R.drawable.cards_discover)
    }

    override fun displayImageJcb() {
        creditCardLogo.setImageResource(R.drawable.cards_jcb)
    }

    override fun displayImageVisa() {
        creditCardLogo.setImageResource(R.drawable.cards_visa)
    }

    override fun displayImageMastercard() {
        creditCardLogo.setImageResource(R.drawable.cards_mastercard)
    }

    override fun displayImageGeneric() {
        creditCardLogo.setImageResource(R.drawable.cards_generic_card)
    }

    override fun hideImage() {
    }
}
