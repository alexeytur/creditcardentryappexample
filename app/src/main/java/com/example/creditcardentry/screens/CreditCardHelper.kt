package com.example.creditcardentry.screens

import androidx.annotation.VisibleForTesting



class CreditCardHelper {

    companion object {
        enum class Network {
            AMEX,
            DINERS_CLUB,
            DISCOVER,
            JCB,
            VISA,
            MASTERCARD,
            GENERIC
        }

        const val prefixAmex1 = "23"
        const val prefixAmex2 = "37"
        const val prefixDiners1 = "42"
        const val prefixDiners2 = "49"
        const val prefixDiscover = "6"
        const val prefixJcb = "3"
        const val prefixVisa = "4"
        const val prefixMastercard = "5"
    }

    fun isCardNumberValid(cardNumber: CharSequence?) : Boolean {
        if (cardNumber == null || cardNumber.isEmpty()) {
            return false
        }
        return luhnAlgorithm(cardNumber)
    }

    fun getCardNetwork(cardNumber: CharSequence?) : Network {
        if (cardNumber == null || cardNumber.isEmpty()) {
            return Companion.Network.GENERIC
        }
        if (cardNumber.startsWith(prefixAmex1) || cardNumber.startsWith(prefixAmex2)) {
            return Companion.Network.AMEX
        } else if (cardNumber.startsWith(prefixDiners1) || cardNumber.startsWith(prefixDiners2)) {
            return Companion.Network.DINERS_CLUB
        } else if (cardNumber.startsWith(prefixDiscover) ) {
            return Companion.Network.DISCOVER
        } else if (cardNumber.startsWith(prefixJcb) ) {
            return Companion.Network.JCB
        } else if (cardNumber.startsWith(prefixVisa) ) {
            return Companion.Network.VISA
        } else if (cardNumber.startsWith(prefixMastercard) ) {
            return Companion.Network.MASTERCARD
        }

        return Network.GENERIC
    }

    // copied from wikipedia
    private fun luhnAlgorithm(cardNumber: CharSequence): Boolean {
        val digits = cardNumber.map { Character.getNumericValue(it) }.toMutableList()
        for (i in (digits.size - 2) downTo 0 step 2) {
            var value = digits[i] * 2
            if (value > 9) {
                value = value % 10 + 1
            }
            digits[i] = value
        }
        return digits.sum() % 10 == 0
    }
}