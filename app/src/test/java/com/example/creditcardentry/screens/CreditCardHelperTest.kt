package com.example.creditcardentry.screens

import org.junit.Test

import org.junit.Assert.*

class CreditCardHelperTest {

    var creditCardHelper = CreditCardHelper()

    @Test
    fun testGetCardNetworkValidPrefix() {
        assertEquals(creditCardHelper.getCardNetwork(CreditCardHelper.prefixAmex1), CreditCardHelper.Companion.Network.AMEX)
        assertEquals(creditCardHelper.getCardNetwork(CreditCardHelper.prefixAmex2), CreditCardHelper.Companion.Network.AMEX)

        assertEquals(creditCardHelper.getCardNetwork(CreditCardHelper.prefixDiners1), CreditCardHelper.Companion.Network.DINERS_CLUB)
        assertEquals(creditCardHelper.getCardNetwork(CreditCardHelper.prefixDiners2), CreditCardHelper.Companion.Network.DINERS_CLUB)

        assertEquals(creditCardHelper.getCardNetwork(CreditCardHelper.prefixJcb), CreditCardHelper.Companion.Network.JCB)
        assertEquals(creditCardHelper.getCardNetwork(CreditCardHelper.prefixDiscover), CreditCardHelper.Companion.Network.DISCOVER)
        assertEquals(creditCardHelper.getCardNetwork(CreditCardHelper.prefixMastercard), CreditCardHelper.Companion.Network.MASTERCARD)
        assertEquals(creditCardHelper.getCardNetwork(CreditCardHelper.prefixVisa), CreditCardHelper.Companion.Network.VISA)
    }

    @Test
    fun testGetCardNetworkInvalidPrefix() {
        assertEquals(creditCardHelper.getCardNetwork(""), CreditCardHelper.Companion.Network.GENERIC)
        assertEquals(creditCardHelper.getCardNetwork(null), CreditCardHelper.Companion.Network.GENERIC)
        assertEquals(creditCardHelper.getCardNetwork("qq"), CreditCardHelper.Companion.Network.GENERIC)
    }
}