package com.example.creditcardentry.screens

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mockito.*
import kotlin.concurrent.timer

class CreditCardEntryPresenterTest {

    private lateinit var view: CreditCardContract.View
    private lateinit var presenter: CreditCardEntryPresenter

    @Before
    fun setUp() {
        view = mock(CreditCardContract.View::class.java)
        presenter = spy(CreditCardEntryPresenter(view))
    }

    @Test
    fun isCreditNumberLastCharValidHappyPath() {
        assertTrue(presenter.isCreditNumberLastCharValid("1234"))
        assertTrue(presenter.isCreditNumberLastCharValid("12"))
    }

    @Test
    fun isCreditNumberLastCharValidWrongChar() {
        assertFalse(presenter.isCreditNumberLastCharValid("1234 "))
        assertFalse(presenter.isCreditNumberLastCharValid("12_"))
        assertFalse(presenter.isCreditNumberLastCharValid("12."))
        assertFalse(presenter.isCreditNumberLastCharValid(""))
        assertFalse(presenter.isCreditNumberLastCharValid(null))
    }

    @Test
    fun creditNumberStringUpdatedGenericMaxNumber() {
        presenter.creditNumberStringUpdated("1234567")
        verify(view, times(1)).creditNumberSetText("123456")
    }

    @Test
    fun creditNumberStringUpdatedAmexMaxNumber() {
        presenter.creditNumberStringUpdated(CreditCardHelper.prefixAmex1 + "23456789123456")
        verify(view, times(1)).creditNumberSetText(CreditCardHelper.prefixAmex1 + "2345678912345")
    }

    @Test
    fun creditNumberStringUpdatedAllNetworksMaxNumber() {
        presenter.creditNumberStringUpdated(CreditCardHelper.prefixAmex1 + "234567891234567")
        verify(view, times(1)).creditNumberSetText(CreditCardHelper.prefixAmex1 + "23456789123456")
    }
}